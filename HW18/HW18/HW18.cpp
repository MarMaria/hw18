﻿// HW18.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <string>

template <typename T>
class Stack
{
private:
	int deltaSize = 256;
	int current_size = 0;
	int current_pos = 0;

	T* stack;

public:
	Stack()
	{
		stack = new T[deltaSize];
		current_size = deltaSize;
	}
	
	void Push(T value)
	{
		if (current_pos == current_size)
		{
			T* stack_new = new T[current_size + deltaSize];

			for (int i = 0; i < current_size; i++)
			{
				stack_new[i] = stack[i];
			}
			delete[] stack;

			stack = stack_new;
			current_size += deltaSize;
		}
		stack[current_pos] = value;
		current_pos++;
	}
	
	T Pop()
	{
		if (current_pos > 0)
		{
			current_pos--;
			return stack[current_pos];
		}
		else
		{
			std::cout << "Stack is empty!\n";
			return NULL;
		}
		return NULL;
	}

	void PrintStack()
	{
		std::cout << "Your Stack: ";
		for (int i = 0; i < current_pos; i++)
		{
			std::cout << stack[i] << ' ';
		}
		std::cout << '\n';
	}
	
	~Stack()
	{
		delete[] stack;
	}


};

int main()
{
	Stack<int> intStack;
	int value;

	while (true)
	{
		char command;
		std::cout << "Push(u) or Pop(o)?\n";
		std::cin >> command;
		if (command == 'u')
		{			
			std::cout << "Push: \n";
			std::cin >> value;
			if (std::cin.fail())
			{
				std::cin.clear();
				std::cin.ignore(256, '\n');
				std::cout << "Uncorrect type!\n";
			}
			else
			{
				intStack.Push(value);
			}
		}
		else if (command == 'o')
		{
			value = intStack.Pop();
			std::cout << "Pop: " << value << '\n';
		}
		else
		{
			std::cout << "Wrong command!\n";
		}

		intStack.PrintStack();
	}

}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
